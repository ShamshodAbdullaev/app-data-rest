package uz.pdp.datarest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.datarest.entity.Company;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqCompany;
import uz.pdp.datarest.repository.CompanyRepository;
import uz.pdp.datarest.service.CompanyServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/company")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final CompanyServiceImpl companyService;

    public CompanyController(CompanyRepository companyRepository, CompanyServiceImpl companyService) {
        this.companyRepository = companyRepository;
        this.companyService = companyService;
    }

    @PostMapping("/add")
    public HttpEntity<?> addCompany(@RequestBody ReqCompany reqCompany){
        ApiResponse response = companyService.addCompany(reqCompany);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> editCompany(@PathVariable UUID id,@RequestBody ReqCompany reqCompany){
        ApiResponse response = companyService.editCompany(id, reqCompany);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteCompany(@PathVariable UUID id){
        ApiResponse response = companyService.deleteCompany(id);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.OK:HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCompany(@PathVariable UUID id){
        Company company = companyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCompany"));
        return ResponseEntity.ok(companyService.getCompany(company));
    }

    @GetMapping("/list")
    public HttpEntity<?> getCompanies(){
        return ResponseEntity.ok(companyService.getCompanies());
    }
}
