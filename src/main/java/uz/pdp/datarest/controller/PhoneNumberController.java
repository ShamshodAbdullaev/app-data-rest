package uz.pdp.datarest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.datarest.entity.PhoneNumber;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqPhoneNumber;
import uz.pdp.datarest.repository.PhoneNumberRepository;
import uz.pdp.datarest.service.PhoneNumberService;
import uz.pdp.datarest.service.PhoneNumberServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/phoneNumber")
public class PhoneNumberController {
    private final PhoneNumberRepository phoneNumberRepository;
    private final PhoneNumberServiceImpl phoneNumberService;

    public PhoneNumberController(PhoneNumberRepository phoneNumberRepository, PhoneNumberServiceImpl phoneNumberService) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.phoneNumberService = phoneNumberService;
    }

    @PostMapping("/add")
    public HttpEntity<?> addPhoneNumber(@RequestBody ReqPhoneNumber reqPhoneNumber){
        ApiResponse response = phoneNumberService.addPhoneNumber(reqPhoneNumber);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> editPhoneNumber(@PathVariable UUID id, @RequestBody ReqPhoneNumber reqPhoneNumber){
        ApiResponse response = phoneNumberService.editPhoneNumber(id, reqPhoneNumber);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deletePhoneNumber(@PathVariable UUID id){
        ApiResponse response = phoneNumberService.deletePhoneNumber(id);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.OK:HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getPhoneNumber(@PathVariable UUID id){
        PhoneNumber phoneNumber = phoneNumberRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getPhoneNumber"));
        return ResponseEntity.ok(phoneNumberService.getPhoneNumber(phoneNumber));
    }

    @GetMapping("/list")
    public HttpEntity<?> getPhoneNumbers(){
        return ResponseEntity.ok(phoneNumberService.getPhoneNumbers());
    }
}
