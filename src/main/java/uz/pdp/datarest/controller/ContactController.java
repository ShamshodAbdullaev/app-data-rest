package uz.pdp.datarest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.datarest.entity.Contact;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqContact;
import uz.pdp.datarest.repository.ContactRepository;
import uz.pdp.datarest.service.ContactServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/contact")
public class ContactController {
    private final ContactRepository contactRepository;
    private final ContactServiceImpl contactService;

    public ContactController(ContactRepository contactRepository, ContactServiceImpl contactService) {
        this.contactRepository = contactRepository;
        this.contactService = contactService;
    }

    @PostMapping("/add")
    public HttpEntity<?> addContact(@RequestBody ReqContact reqContact){
        ApiResponse response = contactService.addContact(reqContact);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> editContact(@PathVariable UUID id, @RequestBody ReqContact reqContact){
        ApiResponse response = contactService.editContact(id, reqContact);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteContact(@PathVariable UUID id){
        ApiResponse response = contactService.deleteContact(id);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.OK:HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getContact(@PathVariable UUID id){
        Contact contact = contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getContact"));
        return ResponseEntity.ok(contactService.getContact(contact));
    }

    @GetMapping("/list")
    public HttpEntity<?> getContacts(){
        return ResponseEntity.ok(contactService.getContacts());
    }

}
