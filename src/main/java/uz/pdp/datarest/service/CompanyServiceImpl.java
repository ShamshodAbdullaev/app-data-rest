package uz.pdp.datarest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.datarest.entity.Company;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqCompany;
import uz.pdp.datarest.repository.CompanyRepository;
import uz.pdp.datarest.repository.ContactRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService{
    private final CompanyRepository companyRepository;
    private final ContactRepository contactRepository;
    final
    CompanyService companyService;

    public CompanyServiceImpl(CompanyRepository companyRepository, ContactRepository contactRepository, CompanyService companyService) {
        this.companyRepository = companyRepository;
        this.contactRepository = contactRepository;
        this.companyService = companyService;
    }

    @Override
    public ApiResponse addCompany(ReqCompany reqCompany) {
        try {
            Company company=new Company();
            company.setName(reqCompany.getName());
            company.setBrand(reqCompany.getBrand());
            company.setTin(reqCompany.getTin());
            company.setContact(contactRepository.getOne(reqCompany.getContactId()));
            companyRepository.save(company);
            return new ApiResponse("Company ro'yhatdan o'tkazildi!!!",true);
        }catch (Exception e){
            return new ApiResponse("Bunday Company mavjud!!!",false);
        }
    }

    @Override
    public ApiResponse editCompany(UUID id, ReqCompany reqCompany) {
        try {
            Optional<Company> optionalCompany=companyRepository.findById(id);
            if (optionalCompany.isPresent()){
                Company company=optionalCompany.get();
                company.setName(reqCompany.getName());
                company.setBrand(reqCompany.getBrand());
                company.setTin(reqCompany.getTin());
                company.setContact(contactRepository.getOne(reqCompany.getContactId()));
                companyRepository.save(company);
                return new ApiResponse("Company o'zgartirildi!!!",true);
            }
            return new ApiResponse("Company topilmadi!!!",false);
        }catch (Exception e){
            return new ApiResponse("Bunday Company ro'yxatdan o'tkazilgan!!!",false);
        }
    }

    @Override
    public ApiResponse deleteCompany(UUID id) {
        if (companyRepository.existsById(id)){
            companyRepository.deleteById(id);
            return new ApiResponse("Company o'chirildi!!!",true);
        }
        return new ApiResponse("Company topilmadi!!!",false);
    }

    @Override
    public ReqCompany getCompany(Company company) {
        return new ReqCompany(
                company.getId(),
                company.getName(),
                company.getBrand(),
                company.getTin(),
                company.getContact().getId()
        );
    }

    @Override
    public List<ReqCompany> getCompanies() {
        return companyRepository.findAll().stream().map(this::getCompany).collect(Collectors.toList());
    }
}
