package uz.pdp.datarest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.datarest.entity.Contact;
import uz.pdp.datarest.entity.template.AbsEntity;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqContact;
import uz.pdp.datarest.payload.ResContact;
import uz.pdp.datarest.repository.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl implements ContactService{
    final
    PhoneNumberRepository phoneNumberRepository;
    private final CountryRepository countryRepository;
    private final RegionRepository regionRepository;
    private final DistrictRepository districtRepository;
    private final ContactRepository contactRepository;

    public ContactServiceImpl(PhoneNumberRepository phoneNumberRepository, CountryRepository countryRepository, RegionRepository regionRepository, DistrictRepository districtRepository, ContactRepository contactRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.countryRepository = countryRepository;
        this.regionRepository = regionRepository;
        this.districtRepository = districtRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public ApiResponse addContact(ReqContact reqContact) {
        try {
            Contact contact = new Contact();
            contact.setCountry(countryRepository.getOne(reqContact.getCountryId()));
            contact.setRegion(regionRepository.getOne(reqContact.getRegionId()));
            contact.setDistrict(districtRepository.getOne(reqContact.getDistrictId()));
            contact.setStreet(reqContact.getStreet());
            contact.setEmail(reqContact.getEmail());
            contact.setHouseNumber(reqContact.getHouseNumber());
            contact.setLat(reqContact.getLat());
            contact.setLan(reqContact.getLan());
            contactRepository.save(contact);
            return new ApiResponse("Contact ro'yhatdan o'tkazildi!!!",true);
        }catch (Exception e){
            return new ApiResponse("Bunday Contact mavjud!!!",false);
        }
    }

    @Override
    public ApiResponse editContact(UUID id, ReqContact reqContact) {
        try {
            Optional<Contact> optionalContact = contactRepository.findById(id);
            if (optionalContact.isPresent()){
                Contact contact = optionalContact.get();
                contact.setCountry(countryRepository.getOne(reqContact.getCountryId()));
                contact.setRegion(regionRepository.getOne(reqContact.getRegionId()));
                contact.setDistrict(districtRepository.getOne(reqContact.getDistrictId()));
                contact.setStreet(reqContact.getStreet());
                contact.setEmail(reqContact.getEmail());
                contact.setHouseNumber(reqContact.getHouseNumber());
                contact.setLat(reqContact.getLat());
                contact.setLan(reqContact.getLan());
                contactRepository.save(contact);
                return new ApiResponse("Contact o'zgartirildi!!!",true);
            }
            return new ApiResponse("Contact topilmadi!!!",false);
        }catch (Exception e){
            return new ApiResponse("Bunday Contact ro'yxatdan o'tkazilgan!!!",false);
        }
    }

    @Override
    public ApiResponse deleteContact(UUID id) {
        if (contactRepository.existsById(id)){
            contactRepository.deleteById(id);
            return new ApiResponse("Contact o'chirildi!!!",true);
        }
        return new ApiResponse("Contact topilmadi!!!",false);
    }

    @Override
    public ResContact getContact(Contact contact) {
        return new ResContact(
                contact.getId(),
                contact.getCountry().getId(),
                contact.getRegion().getId(),
                contact.getDistrict().getId(),
                contact.getStreet(),
                contact.getEmail(),
                contact.getHouseNumber(),
                contact.getLat(),
                contact.getLan()
        );
    }

    @Override
    public List<ResContact> getContacts() {
        return contactRepository.findAll().stream().map(this::getContact).collect(Collectors.toList());
    }
}
