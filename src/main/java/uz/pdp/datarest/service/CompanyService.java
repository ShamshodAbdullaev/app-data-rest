package uz.pdp.datarest.service;

import uz.pdp.datarest.entity.Company;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqCompany;

import java.util.List;
import java.util.UUID;

public interface CompanyService {
    ApiResponse addCompany(ReqCompany reqCompany);

    ApiResponse editCompany(UUID id, ReqCompany reqCompany);

    ApiResponse deleteCompany(UUID id);

    ReqCompany getCompany(Company company);

    List<ReqCompany> getCompanies();

}
