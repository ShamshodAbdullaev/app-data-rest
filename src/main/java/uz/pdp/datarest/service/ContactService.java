package uz.pdp.datarest.service;

import uz.pdp.datarest.entity.Contact;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqContact;
import uz.pdp.datarest.payload.ResContact;

import java.util.List;
import java.util.UUID;

public interface ContactService {

    ApiResponse addContact(ReqContact reqContact);

    ApiResponse editContact(UUID id, ReqContact reqContact);

    ApiResponse deleteContact(UUID id);

    ResContact getContact(Contact contact);

    List<ResContact> getContacts();
}
