package uz.pdp.datarest.service;

import uz.pdp.datarest.entity.PhoneNumber;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqPhoneNumber;
import uz.pdp.datarest.payload.ResPhoneNumber;

import java.util.List;
import java.util.UUID;

public interface PhoneNumberService {

    ApiResponse addPhoneNumber(ReqPhoneNumber reqPhoneNumber);

    ApiResponse editPhoneNumber(UUID id,ReqPhoneNumber reqPhoneNumber);

    ApiResponse deletePhoneNumber(UUID id);

    ResPhoneNumber getPhoneNumber(PhoneNumber phoneNumber);

    List<ResPhoneNumber> getPhoneNumbers();

}
