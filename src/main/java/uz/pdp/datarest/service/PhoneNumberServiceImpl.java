package uz.pdp.datarest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.datarest.entity.PhoneNumber;
import uz.pdp.datarest.payload.ApiResponse;
import uz.pdp.datarest.payload.ReqPhoneNumber;
import uz.pdp.datarest.payload.ResPhoneNumber;
import uz.pdp.datarest.repository.ContactRepository;
import uz.pdp.datarest.repository.PhoneNumberRepository;
import uz.pdp.datarest.repository.PhoneNumberTypeRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {
    private final PhoneNumberRepository phoneNumberRepository;
    private final PhoneNumberTypeRepository phoneNumberTypeRepository;
    private final ContactRepository contactRepository;

    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository, PhoneNumberTypeRepository phoneNumberTypeRepository, ContactRepository contactRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
        this.phoneNumberTypeRepository = phoneNumberTypeRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public ApiResponse addPhoneNumber(ReqPhoneNumber reqPhoneNumber) {
        try {
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setPhoneNumberType(phoneNumberTypeRepository.getOne(reqPhoneNumber.getPhoneNumberTypeId()));
            phoneNumber.setPhoneNumber(reqPhoneNumber.getPhoneNumber());
            phoneNumber.setContact(contactRepository.getOne(reqPhoneNumber.getContactId()));
            phoneNumberRepository.save(phoneNumber);
            return new ApiResponse("PhoneNumber ro'yhatdan o'tkazildi!!!",true);
        }catch (Exception e){
            return new ApiResponse("Bunday PhoneNumber mavjud!!!",false);
        }
    }

    @Override
    public ApiResponse editPhoneNumber(UUID id, ReqPhoneNumber reqPhoneNumber) {
        try {
            Optional<PhoneNumber> optionalPhoneNumber=phoneNumberRepository.findById(id);
            if (optionalPhoneNumber.isPresent()){
                PhoneNumber phoneNumber=optionalPhoneNumber.get();
                phoneNumber.setPhoneNumberType(phoneNumberTypeRepository.getOne(reqPhoneNumber.getPhoneNumberTypeId()));
                phoneNumber.setPhoneNumber(reqPhoneNumber.getPhoneNumber());
                phoneNumber.setContact(contactRepository.getOne(reqPhoneNumber.getContactId()));
                phoneNumberRepository.save(phoneNumber);
                return new ApiResponse("PhoneNumber o'zgartirildi!!!",true);
            }
            return new ApiResponse("PhoneNumber topilmadi!!!",false);
        }catch (Exception e){
            return new ApiResponse("Bunday PhoneNumber ro'yxatdan o'tkazilgan!!!",false);
        }
    }

    @Override
    public ApiResponse deletePhoneNumber(UUID id) {
        if (phoneNumberRepository.existsById(id)){
            phoneNumberRepository.deleteById(id);
            return new ApiResponse("PhoneNumber o'chirildi!!!",true);
        }
        return new ApiResponse("PhoneNumber topilmadi!!!",false);
    }

    @Override
    public ResPhoneNumber getPhoneNumber(PhoneNumber phoneNumber) {
        return new ResPhoneNumber(
                phoneNumber.getId(),
                phoneNumber.getPhoneNumberType().getId(),
                phoneNumber.getPhoneNumber(),
                phoneNumber.getContact().getId()
        );
    }

    @Override
    public List<ResPhoneNumber> getPhoneNumbers() {
        return phoneNumberRepository.findAll().stream().map(this::getPhoneNumber).collect(Collectors.toList());
    }
}
