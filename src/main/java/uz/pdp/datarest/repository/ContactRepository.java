package uz.pdp.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.datarest.entity.Contact;

import java.util.UUID;

public interface ContactRepository extends JpaRepository<Contact, UUID> {
    boolean existsById(UUID id);
}
