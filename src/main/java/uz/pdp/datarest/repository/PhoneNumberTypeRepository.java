package uz.pdp.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.datarest.entity.PhoneNumberType;
import uz.pdp.datarest.projection.CustomPhoneNumberType;

@RepositoryRestResource(path = "phoneNumberType",excerptProjection = CustomPhoneNumberType.class)
public interface PhoneNumberTypeRepository extends JpaRepository<PhoneNumberType,Integer> {
}
