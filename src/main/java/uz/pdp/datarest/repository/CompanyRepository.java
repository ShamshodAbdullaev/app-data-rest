package uz.pdp.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.datarest.entity.Company;

import java.util.UUID;

public interface CompanyRepository extends JpaRepository<Company, UUID> {
    boolean existsById(UUID id);
}
