package uz.pdp.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.datarest.entity.PhoneNumber;

import java.util.UUID;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, UUID> {
    boolean existsById(UUID id);
}
