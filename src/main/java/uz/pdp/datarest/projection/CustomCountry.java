package uz.pdp.datarest.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.datarest.entity.Country;

@Projection(name = "customCountry",types = Country.class)
public interface CustomCountry {

    Integer getId();

    String getNameUz();
    String getNameRu();
    String getNameEn();
}
