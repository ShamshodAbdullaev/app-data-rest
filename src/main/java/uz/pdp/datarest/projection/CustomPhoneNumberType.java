package uz.pdp.datarest.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.datarest.entity.PhoneNumberType;

@Projection(name = "customPhoneNumberType",types = PhoneNumberType.class)
public interface CustomPhoneNumberType {

    Integer getId();

    String getNameUz();
    String getNameRu();
    String getNameEn();
}
