package uz.pdp.datarest.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqCompany {
    private UUID id;
    private String name;
    private String brand;
    private String tin;
    private UUID contactId;
}
