package uz.pdp.datarest.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPhoneNumber {
    private UUID id;
    private Integer phoneNumberTypeId;
    private String phoneNumber;
    private UUID contactId;
}
