package uz.pdp.datarest.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResContact {
    private UUID id;
    private Integer countryId;
    private Integer regionId;
    private Integer districtId;
    private String street;
    private String email;
    private String houseNumber;
    private String lat;
    private String lan;
}
